//
//  CollectionViewDelegate.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/18/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import Foundation

protocol CollectionViewDelegate: NSObjectProtocol {
    func displayGetPhotos()
    func displayAlertError(error: NSError)
}

extension CollectionViewDelegate {
    func displayAlertError(title: String, message: String){}
}

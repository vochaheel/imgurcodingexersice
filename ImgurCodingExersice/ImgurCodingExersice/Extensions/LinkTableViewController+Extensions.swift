//
//  LinkTableViewController+Extensions.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/21/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import UIKit

extension LinkTableViewController {
    
    typealias ActionHandler = () -> Void

    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
    }
    
    func showAlert(title: String, message: String, completion: @escaping ActionHandler ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
            completion()
        })
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

//
//  UICollectionViewController+Extension.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/18/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import UIKit

extension MainCollectionViewController {
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
    }
}

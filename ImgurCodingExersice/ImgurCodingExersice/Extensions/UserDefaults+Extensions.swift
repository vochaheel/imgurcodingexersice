//
//  UserDefaults+Extensions.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/20/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import Foundation

extension UserDefaults {
    func contains(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func saveKey(value: [String], forKey key: String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func loadKey(forKey key: String) -> [String] {
        return UserDefaults.standard.object(forKey: key) as! [String]
    }
    
}

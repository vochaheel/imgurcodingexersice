//
//  ImageCollectionViewCell.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/18/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userImage: UIImageView!
    
    func initActivityIndicator() {
        activityIndicator.style = .whiteLarge
        activityIndicator.isHidden = true
    }
    
    func startActivityIndicator() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
    }
}

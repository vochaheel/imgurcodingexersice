//
//  PhotosManager.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/18/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import Photos
import UIKit

class PhotosManager {
    
    static let shared = PhotosManager()
    
    private init(){}
    
    weak var collectionViewDelegate: CollectionViewDelegate?
    
    func getPhotofromGallery(completion: @escaping (_ image: UIImage) -> Void, error: @escaping () -> Void) {
        let imgManager = PHImageManager.default()
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false) ]
        
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        // Если в галлереи присутствуют фото
        if fetchResult.count > 0 {
            for i in 0..<fetchResult.count {
                imgManager.requestImage(for: fetchResult.object(at: i), targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: requestOptions) { image, error in
                    // Добавляем в массив полученное фото
                    completion(image!)
                }
            }
        } else {
            self.collectionViewDelegate?.displayAlertError(title: "Loading Error", message: "You got no photos!")
            DispatchQueue.main.async {
                error()
            }
        }
    }
}


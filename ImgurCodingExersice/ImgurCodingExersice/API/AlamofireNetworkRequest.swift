//
//  AlamofireNetworkRequest
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/18/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//
import UIKit
import Alamofire

class AlamofireNetworkRequest {
    
    static let shared = AlamofireNetworkRequest()
    
    private init(){}
    
    weak var collectionViewDelegate: CollectionViewDelegate?
    
    private let uploadImageUrl = "https://api.imgur.com/3/image"
    private let userDefaults = UserDefaults.standard
    private var imageLinkArray: [String] = []
    
    func uploadImage(image: UIImage, completion: @escaping ()->()) {
        // создание глобальной очереди с асинхронным выполнением задач с приоритетом utility
        let queue = DispatchQueue.global(qos: .utility)
        queue.async {
            if let currentImageLinkArray = self.userDefaults.object(forKey: defaultsKeys.imageLinkArrayKey) as! [String]? {
                self.imageLinkArray = currentImageLinkArray
            }
            // проверка на существование id
            guard let id = ProcessInfo.processInfo.environment["Client-ID"] else {return}
            // проверка на существование ссылки
            guard let url = URL(string: self.uploadImageUrl) else { return }
            // конвертация изображение в data
            let data = image.pngData()!
            // параметры авторизации
            let httpHeaders = ["Authorization": "Client-ID " + id]
            // загрузка через multipartFormData, который предназначен для создания данных из составных частей для передачи данных по http
            upload(multipartFormData: { (multipartFormData) in
                // передаем объект data с параметром, ключем "image"
                multipartFormData.append(data, withName: "image")
                
            }, to: url, headers: httpHeaders) { (encodingCompletion) in
                // encodingCompletion - закодированный запрос, который к нам пришел
                switch encodingCompletion {
                // данные подготавливаются  и записываются на диск и будут переданы в виде ссылки
                case .success(request: let uploadRequest,
                              streamingFromDisk: let streamingFromDisk,
                              streamFileURL: let streamFileURL):
                    
                    print(uploadRequest)
                    print(streamingFromDisk)
                    print(streamFileURL ?? "strimingFileURL is NIL")
                    // запрос на сервер с валидацией
                    uploadRequest.validate().responseJSON(completionHandler: { (responseJSON) in
                        
                        switch responseJSON.result {
                            
                        case .success(let value):
                            // привидение полученного значения к словарю
                            let json = value as? [String: Any]
                            // проверки на существование ключей
                            guard let imageDic = json?[ImgurAPIResponse.data] as? [String: Any] else { return }
                            guard let imageLink = imageDic[ImgurAPIResponse.link] else { return }
                            // добавление полученной ссылки в массив
                            self.imageLinkArray.append(imageLink as! String)
                            // сохранение массива при получении ссылки
                            self.userDefaults.saveKey(value: self.imageLinkArray, forKey: defaultsKeys.imageLinkArrayKey)
                            // замыкание служит для того, чтобы остановить спиннер
                            DispatchQueue.main.async {
                                completion()
                            }
                        case .failure(let error):
                            // отображение ошибки
                            self.collectionViewDelegate?.displayAlertError(error: error as NSError)
                        }
                    })
                    
                case .failure(let error):
                    // отображение ошибки
                    self.collectionViewDelegate?.displayAlertError(error: error as NSError)
                }
            }
        }
    }
    
}

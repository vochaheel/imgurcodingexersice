//
//  DefaultsKeys.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/20/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import Foundation

struct defaultsKeys {
    static let imageLinkArrayKey = "SavedImageLinkArray"
}

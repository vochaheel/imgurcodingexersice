//
//  ImgurAPIResponse.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/20/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import Foundation

public struct ImgurAPIResponse {
    static let data = "data"
    static let link = "link"
}

public struct ImageLink {
    public let link: String
}

//
//  MainCollectionViewController.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/17/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import UIKit
import Photos

private let reuseIdentifier = "ImageCell"

class MainCollectionViewController: UICollectionViewController, CollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    private var  links: [ImageLink] = []
    private let minimumSpacingForSection: CGFloat = 15.0
    private var imageArray = [UIImage]()
    // получение фото из галлереи
    func displayGetPhotos() {
        PhotosManager.shared.getPhotofromGallery(completion: { image in
            self.imageArray.append(image)
        }) {
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.alwaysBounceVertical = true
        // для того, что бы collectionView не залезал на элементы управления
        collectionView.contentInsetAdjustmentBehavior = .always
        displayGetPhotos()
    }
    
    func displayAlertError(error: NSError) {
        showAlert(title: "Error", message: error.localizedDescription)
    }
    
    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
        cell.userImage.image = imageArray[indexPath.row]
        cell.initActivityIndicator()
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
        // при нажатии включаю спиннер и передаю текущее изображение на отправку на сервер
        cell.startActivityIndicator()
        let image = imageArray[indexPath.item]
        AlamofireNetworkRequest.shared.uploadImage(image: image) { 
            cell.activityIndicator.stopAnimating()
        }
    }
    
    // MARK: UICollectionViewDelegate
    
    // минимальный междустрочный интервал
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumSpacingForSection
    }
    // минимальный интервал между элементами
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumSpacingForSection
    }
    // размер ячейки указанного элемента
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let safeFrame = view.safeAreaLayoutGuide.layoutFrame
        let size = CGSize(width: safeFrame.width, height: safeFrame.height)
        return setCollectionViewItemSize(size: size)
    }
    // срабатывает при изменении ориентации экрана
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            // обновление информации макета, предварительно блокируя текущий
            layout.invalidateLayout()
        }
    }
    // определяем количество элементов, в зависимости от ориентации экрана
    func setCollectionViewItemSize(size: CGSize) -> CGSize {
        if UIApplication.shared.statusBarOrientation.isPortrait {
            let width = (size.width - 1 * minimumSpacingForSection) / 4
            return CGSize(width: width, height: width)
        } else {
            let width = (size.width - 2 * minimumSpacingForSection) / 6
            return CGSize(width: width, height: width)
        }
    }
}

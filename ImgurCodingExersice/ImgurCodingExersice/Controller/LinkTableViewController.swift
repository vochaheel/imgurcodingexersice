//
//  LinkTableViewController.swift
//  ImgurCodingExersice
//
//  Created by Constantine Likhachov on 7/19/19.
//  Copyright © 2019 Constantine Likhachov. All rights reserved.
//

import UIKit

private let reuseIdentifier = "LinkCell"

class LinkTableViewController: UITableViewController {
    
    let userDefaults = UserDefaults.standard
    var link: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if userDefaults.contains(key: defaultsKeys.imageLinkArrayKey) {
            link = userDefaults.loadKey(forKey: defaultsKeys.imageLinkArrayKey)
        } else {
            showAlert(title: "Error", message: "Please, upload a photo to get links") {
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return link.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        cell.textLabel?.text = link[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // при нажатии на ячейку, открываем ссылку в собсвтенном браузере
        let url = link[indexPath.row]
        if let url = URL(string: url), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
